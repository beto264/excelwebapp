package cl.dev.excelwebapp;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import javax.servlet.ServletContext;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

@Controller
public class UploadController {

  @Autowired
  ServletContext context;

  @PostMapping("/upload")
  public String upload(@RequestParam("file") InputStreamSource file, Map<String, Object> model) {


    //TO DO:

    //llamamos al service que hace la lectura del excel
    //traemos los datos del excel
    //si leemos los datos correctamentem redireccionamos a:
    //si no leemos los datos correctamentem redireccionamos a:

    List<String> stringList = new ArrayList<>();

    try {

      Workbook workbook = new XSSFWorkbook(file.getInputStream());
      Sheet datatypeSheet = workbook.getSheetAt(0);
      Iterator<Row> iterator = datatypeSheet.iterator();

      while (iterator.hasNext()) {

        Row currentRow = iterator.next();
        Iterator<Cell> cellIterator = currentRow.iterator();

        while (cellIterator.hasNext()) {

          Cell currentCell = cellIterator.next();

          //leemos celda a celda
          //como ya viene en formato de excel, solamente creamos un string linea a linea

          if (currentCell.getCellTypeEnum() == CellType.STRING) {
            stringList.add(currentCell.getStringCellValue() + "--");
          } else if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
            stringList.add(currentCell.getNumericCellValue() + "--");
          }

        }

        //hardcoded salto de linea :P

        stringList.add("<br>");

      }
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }


    model.put("data", stringList);

    return "upload";
  }


}
