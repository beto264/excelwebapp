<%--
  Created by IntelliJ IDEA.
  User: beto
  Date: 2019-04-22
  Time: 10:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Upload</title>
</head>
<body>

<div>
    <table style="border: 1px solid black;">
        <c:forEach var="entry" items="${data}">
            <tr>${entry}</tr>
        </c:forEach>
    </table>
</div>

</body>
</html>
