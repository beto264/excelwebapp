<!DOCTYPE html>

<head>
    <meta charset="UTF-8">
</head>

<body>
<h3>File Upload:</h3>
Select a file to upload: <br/>
<form action="${pageContext.request.contextPath}/upload" method="post"
      enctype="multipart/form-data">
    <input type="file" name="file" size="10" accept=".xlsx, .xls"/>
    <br/>
    <input type="submit" value="Upload File"/>
</form>
</body>