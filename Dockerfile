FROM tomcat:9.0.19-jre8-slim

ENV CATALINA_HOME /usr/local/tomcat
ENV SERVER_PORT 8080

## Configuration (users and roles)
COPY tomcat-users.xml $CATALINA_HOME/conf/
COPY context.xml $CATALINA_HOME/webapps/manager/META-INF/context.xml

#Add the war
COPY ./target/excelwebapp-1.0.war $CATALINA_HOME/webapps/

EXPOSE $SERVER_PORT

# Launch Tomcat on startup
CMD ["catalina.sh", "run"]