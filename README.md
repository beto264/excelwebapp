# excelwebapp-1.0

### Contexto del proyecto

Spring Boot (backend) + JSP (frontend) con lógica de negocio para cargar un archivo excel y desplegar su información 
en el navegador.

Como backend se puede utilizar cualquier otro tipo de Servidor de aplicaciones compatible con Java.


### Pre requisitos

#### Modificación y/o compilación 

* [Git](https://git-scm.com/downloads) - Software Version Control
* [Maven](https://maven.apache.org/) - Dependency Management
* Libreria necesaria para la lectura de información desde un archivo Excel:

````

    <dependency>
        <groupId>org.apache.poi</groupId>
        <artifactId>poi-ooxml</artifactId>
        <version>3.15</version>
    </dependency>

````

### Tomcat

#### Configuración

Para la configuración del servidor de aplicación a nivel de usuarios y roles se utiliza el archivo:

* tomcat-users.xml

#### Endpoints

Se han configurado los siguientes endpoints:

* Home: http://<host>:<puerto>/excelwebapp-1.0/home 
* Upload: http://<host>:<puerto>/excelwebapp-1.0/upload 


#### Ejecución del proyecto dockerizado

* [Docker](https://www.docker.com/get-started) - Container Platform


## Ejecución local

Para la ejecución del proyecto son necesarios los siguientes pasos:

* Para generar el JAR:

````
mvn clean install
````

* Para generar el JAR y levantar spring boot:

````
mvn clean install spring-boot:run
````

Luego nuestro proyecto web queda expuesto en el puerto por defecto 8080. Para modificar el puerto debemos editar el 
archivo application.properties añadiendo:

````
server.port = <puerto>
````

## Ejecución proyecto dockerizado

Para la ejecución del proyecto son necesarios los siguientes pasos:

* Generar el WAR:

````
mvn clean package
````

* Crear la imágen:

````
docker build -t <nombre-imagen> .

Ejemplo:

docker build -t excelwebapp .
````

### Ejecución local del contenedor

````
docker run -it -d -p <puerto-externo>:<puerto-interno> <nombre-imagen>

Ejemplo: 

docker run -it -d -p 8080:8080 excelwebapp
````

## Push de imágen

* Login en Docker Hub:

````
docker login
````

* Creamos el tag de nuestra imágen:

Esto es opcional, pero nos permite controlar las versiones.

````
docker tag <nombre-imagen> <docker-id>/<nombre-imagen>:version

Ejemplo: 

docker tag excelwebapp albertotejos/excelwebapp:1.0

````

* Subimos a Docker Hub nuestra imágen:

````
docker push <docker-id>/<nombre-imagen>:version

Ejemplo: 

docker push albertotejos/excelwebapp:1.0
````

## Pull de imágen

* Para realizar la descarga de la imágen, ejecutamos:

````
docker pull <docker-id>/<nombre-imagen>:version

Ejemplo: 

docker pull albertotejos/excelwebapp:1.0
````


## Construido con

* [Spring Boot](https://spring.io/projects/spring-boot) - Spring Framework Tool
* [Maven](https://maven.apache.org/) - Dependency Management

## Autores

* **Alberto Tejos S.** - *Trabajo inicial*

## Licencia

Este proyecto está licenciado bajo la licencia MIT - ver [LICENSE.md](LICENSE.md) para más detalles

